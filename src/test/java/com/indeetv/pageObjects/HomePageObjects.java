package com.indeetv.pageObjects;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.indeetv.TestBase.TestBase;

public class HomePageObjects {

	private WebDriver driver;

	public HomePageObjects(WebDriver driver) 
	{
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}


	@FindBy(xpath = "//div[@id='nav-logo']/a[@class='nav-logo-link']")
	private WebElement amazomHomePageLogo;

	@FindBy(xpath = "//input[@id='twotabsearchtextbox']")
	private WebElement searchBar;

	@FindBy(xpath = "//div[@id='nav-search']//span[contains(text(),'Go')]")
	private WebElement searchBarSubmitButton;

	@FindBy(xpath = "//div[@id='suggestions']//span[.='Cell Phones & Accessories']")
	private WebElement cellPhonesAndAccessoriesOptionInDropDown;
	
	@FindBy(xpath = "//h1[contains(text(),'Sign-In')]")
	private WebElement signInButton;
	
	
	
	
	public WebElement amazonHomePageLogo() {
		return new WebDriverWait(driver, 60).until(ExpectedConditions.visibilityOf(amazomHomePageLogo));
	}

	public WebElement searchBar() {
		return new WebDriverWait(driver, 60).until(ExpectedConditions.visibilityOf(searchBar));
	}

	public WebElement searchBarSubmitButton() {
		return new WebDriverWait(driver, 60).until(ExpectedConditions.visibilityOf(searchBarSubmitButton));
	}

	public WebElement cellPhonesAndAccessoriesOptionInDropDown() {
		return new WebDriverWait(driver, 60).until(ExpectedConditions.visibilityOf(cellPhonesAndAccessoriesOptionInDropDown));
	}

	public List<WebElement> searchPageResults() {
		return driver.findElements(By.xpath("//span[contains(text(),'Simple Mobile')]"));
	}
	
	public List<WebElement> midNightGreenIphoneSearchResult() {
		return driver.findElements(By.xpath("//span[contains(text(),'Midnight Green')]"));
	}
	
	public WebElement signInHeadingInLoginPage() {
		return new WebDriverWait(driver, 60).until(ExpectedConditions.visibilityOf(signInButton));
	}
	

}
