package com.indeetv.pageObjects;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ProductDeliveryPageObjects {

	WebDriver driver;

	public ProductDeliveryPageObjects(WebDriver driver) {
		System.out.println("initializing the new WebDriverWait(driver, 60)s............");
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	

	@FindBy(xpath = "//span[@id='productTitle' and contains(text(),'Midnight Green')]")
	private WebElement midnightGreenIphoneProductPage;
	
	@FindBy(xpath = "//div[@id='contextualIngressPt']//span[contains(text(),'Deliver to')]")
	private WebElement deliverToTab;
	
	@FindBy(xpath = "//div[contains(@id,'ZipInputSection')]//input[contains(@id,'ZipUpdateInput')]")
	private WebElement zipCodeInputFieldOnChooseLocationPopUp;
		
	@FindBy(xpath = "//div[contains(@id,'ZipInputSection')]//span[contains(text(),'Apply')]")
	private WebElement applyButtonOnChooseLocationPopUp;
	
	@FindBy(xpath = "//div[@class='a-popover-footer']//span[contains(text(),'Continue')]")
	private WebElement continueButtonOnChooseLocationPopUp;
	
	@FindBy(xpath = "//input[@id='add-to-cart-button']/following-sibling::span")
	private WebElement addToCartButton;
	
	@FindBy(xpath = "//span[@id='attachSiNoCoverage']//button[@id='attachSiNoCoverage-announce']")
	private WebElement noThanksButton;
	
	@FindBy(xpath = "//span[@id='attach-sidesheet-view-cart-button-announce']")
	private WebElement cartButton;
	
	@FindBy(xpath = "//form[@id='activeCartViewForm']//span[contains(text(),'Qty')]/following-sibling::span")
	private WebElement quantityInTheCartCount;
	
	@FindBy(xpath = "//span[@id='sc-buy-box-ptc-button-announce']")
	private WebElement proceedToCheckOutButton;
	
	
	
	
	
	
	public WebElement productResultPage()
	{
		return new WebDriverWait(driver, 60).until(ExpectedConditions.visibilityOf(midnightGreenIphoneProductPage));
	}
	
	public WebElement deliverTo()
	{
		return new WebDriverWait(driver, 60).until(ExpectedConditions.visibilityOf(deliverToTab));
	}
	
	public WebElement inputFieldForAddingZipCode()
	{
		return new WebDriverWait(driver, 60).until(ExpectedConditions.visibilityOf(zipCodeInputFieldOnChooseLocationPopUp));
	}
	
	public WebElement applyButtonOnLocationPopUp()
	{
		return new WebDriverWait(driver, 60).until(ExpectedConditions.elementToBeClickable(applyButtonOnChooseLocationPopUp));
	}
	
	public WebElement continueButtonOnLocationPopUp()
	{
		return new WebDriverWait(driver, 60).until(ExpectedConditions.visibilityOf(continueButtonOnChooseLocationPopUp));
	}
	
	public WebElement addToCartButtonOnProductPage()
	{
		return new WebDriverWait(driver, 60).until(ExpectedConditions.elementToBeClickable(addToCartButton));
	}

	public WebElement noThanksButtonOnAdditionAddedProductPage()
	{
		return new WebDriverWait(driver, 60).until(ExpectedConditions.visibilityOf(noThanksButton));
	}
	
	public WebElement cartButtonOnAdditionAddedProductPage()
	{
		return new WebDriverWait(driver, 60).until(ExpectedConditions.elementToBeClickable(cartButton));
	}
	
	public WebElement quantityCountInCart()
	{
		return new WebDriverWait(driver, 60).until(ExpectedConditions.visibilityOf(quantityInTheCartCount));
	}
	
	public WebElement proceedToCheckOutButtonOnProductPage()
	{
		return new WebDriverWait(driver, 60).until(ExpectedConditions.visibilityOf(proceedToCheckOutButton));
	}
}
