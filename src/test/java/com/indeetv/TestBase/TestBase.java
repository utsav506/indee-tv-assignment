package com.indeetv.TestBase;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Platform;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import io.github.bonigarcia.wdm.ChromeDriverManager;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;

public class TestBase {
	
	//public static RemoteWebDriver driver= null;
	public static WebDriver driver = null;
	public static  String deviceType = "Emulator";
	//public static MobileDriver mdriver = null;

	public TestBase() throws MalformedURLException
	{
		System.out.println("From testBase Constructor.....");
		//initializeDriver();
	}
	
	
	
	
	@SuppressWarnings("rawtypes")
	@BeforeSuite(alwaysRun=true)
	public void initializeDriver() throws MalformedURLException
	{
	        
		/*
		 * DesiredCapabilities cap = new DesiredCapabilities();
		 * cap.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR,
		 * UnexpectedAlertBehaviour.ACCEPT); cap.setCapability("disable-popup-blocking",
		 * false);
		 */
	        System.out.println("Setting up device...........................................");
	        if(System.getProperty("browser").equalsIgnoreCase("Chrome") && System.getProperty("osVersion").contains("Android"))
			{
	        
	        		System.out.println("Loading the chrome driver for windows OS ========================");
	        		String url = null;
	        		String port = System.getProperty("port");
	        		if(port == null)
	        		{
	        			url = "http://"+System.getProperty("nodeIP").trim()+":4723/wd/hub";
	        		}else
	        		{
	        			url = "http://"+System.getProperty("nodeIP").trim()+":"+port.trim()+"/wd/hub";
	        		}
	        		
	        		
	        		DesiredCapabilities capabilities = DesiredCapabilities.android();
	        		capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "Emulator");
	        		capabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, 120);
	        		capabilities.setCapability(MobileCapabilityType.PLATFORM, "Windows");
	        		capabilities.setCapability(CapabilityType.BROWSER_NAME, BrowserType.CHROME);
	        		capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, "7.1.1");
	        		capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, Platform.ANDROID);
	        		capabilities.setCapability("noReset",true);
	        		capabilities.setCapability("avd","Emulator");
	        		
	        		//capabilities.setCapability("appPackage", "com.android.appName");
	        		//capabilities.setCapability("appActivity", "com.android.collagemake.MainActivity");
	        		
	        		String pathForChromeDriver = System.getProperty("user.dir")+"/driver/chromedriver.exe";
	        		
	        		capabilities.setCapability("chromedriverExecutable", pathForChromeDriver);
	        		capabilities.setCapability("reboot" , true);
	        		
	        		 ChromeOptions options = new ChromeOptions();
	                 options.addArguments("test-type");
	                 options.addArguments("--disable-dev-shm-usage");
	                 capabilities.setCapability("chrome.binary", pathForChromeDriver);
	                 capabilities.setCapability(ChromeOptions.CAPABILITY, options);
	                 System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"/driver/chromedriver.exe");
	        		
	        		try {
	        				
	        				driver = new AndroidDriver(new URL(url), capabilities);
	        				// driver = (RemoteWebDriver) mdriver;
	        				System.out.println(System.getProperty("browser")+"driver is initiated");
	        		}catch(Exception e)
	        		{
	        			e.printStackTrace();
	        		}
	        		
	        		
	        	System.out.println("Trying to launch the URL");
	     
			}else if(System.getProperty("browser").equalsIgnoreCase("Chrome") && System.getProperty("osVersion").contains("Window"))
	        {
				System.out.println("Loading the chrome driver for windows OS ========================");
				DesiredCapabilities capabilities = DesiredCapabilities.chrome();
				//String pathForChromeDriver = System.getProperty("user.dir")+"/driver/chromedriver.exe";
				//System.setProperty("webdriver.chrome.driver", pathForChromeDriver);
				ChromeDriverManager.getInstance().setup(); 
				ChromeOptions options = new ChromeOptions();
				options.addArguments("--start-maximized");
				capabilities.setCapability(ChromeOptions.CAPABILITY, options);
				driver = new ChromeDriver(capabilities);
				
			
			/*
			 * driver.manage().window().maximize();
			 * driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			 */
				
	        }
	        else
	        {
	        	System.out.println("Please select the chrome browser... because the code is only written for CHROME");
	        }	
		
	}
	
	
	@AfterSuite
	public void quitDriver()
	{
		System.out.println(" QUitting the driver .....");
		driver.quit();
	}
	
}
