package com.indeetv.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.indeetv.stepDefination.HomePage;
import com.indeetv.stepDefination.ProductPage;

public class TestAmazonHomePage {

	HomePage home;
	ProductPage productPage;
	Properties prop = new Properties();
	public static final String pathOFProtiesFile = System.getProperty("user.dir")+"/parameter.properties";
	
	@BeforeClass
	public void initialize() throws FileNotFoundException, IOException
	{
		home = new HomePage();
		productPage = new ProductPage();
		prop.load(new FileInputStream(pathOFProtiesFile));
	}

	@Test( priority = 1)
	public void openAmazonHomePage() {
		int TIMES_ADD_TO_CART = 1;
		int QUANTITY_IN_THE_CART = 0;
		try {
			
			home.openAmazonHomePageScreen(prop.getProperty("url"));

			home.searchProductOnSearchBarAndClickOnSubmitButton(prop.getProperty("productNameToBeSerached"));

			home.checksearchPageResultsAndCompareWithInputs(prop.getProperty("color_varient_1"), prop.getProperty("color_varient_2"));

			productPage.selectProductAndMoveToProductPage(prop.getProperty("color_varient_2"));

			productPage.setZipCode(prop.getProperty("zipCode"));

			QUANTITY_IN_THE_CART =  productPage.addProductToTheCart();
			
			home.checkAmazonLoginPageDisplayed();
			
			if(TIMES_ADD_TO_CART == QUANTITY_IN_THE_CART)
			{
				Assert.assertEquals(true, true, QUANTITY_IN_THE_CART+" quantity in cart is equals to Number of times product added to cart : "+TIMES_ADD_TO_CART);
			}else
			{
				Assert.assertEquals(false, false, QUANTITY_IN_THE_CART+" quantity in cart is NOT EQUAL to Number of times product added to cart : "+TIMES_ADD_TO_CART);	
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	

}
