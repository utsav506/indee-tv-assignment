package com.indeetv.stepDefination;

import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import org.apache.http.util.Asserts;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

import com.indeetv.TestBase.TestBase;
import com.indeetv.pageObjects.HomePageObjects;
import com.indeetv.pageObjects.ProductDeliveryPageObjects;

import junit.framework.Assert;

public class ProductPage {

	WebDriver driver;
	HomePageObjects homePageObjects;
	ProductDeliveryPageObjects productPageObjects;

	public ProductPage() {
		this.driver = TestBase.driver;
		homePageObjects = new HomePageObjects(driver);
		productPageObjects = new ProductDeliveryPageObjects(driver);
		// TestBase.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	public void selectProductAndMoveToProductPage(String colorVarientMidnightGreen) {

		try {
			if (homePageObjects.midNightGreenIphoneSearchResult().size() > 0) {
				System.out.println("MidNightGreen color varient is available");
				homePageObjects.midNightGreenIphoneSearchResult().get(0).click();
				try {
					if (productPageObjects.productResultPage().isDisplayed()) {
						System.out.println("Product result page with iphone color varient " + colorVarientMidnightGreen
								+ " is opened sucessfully");
						Assert.assertEquals("Product result page with iphone color varient " + colorVarientMidnightGreen
								+ " is opened sucessfully", true, true);
					}
				} catch (Exception e) {
					Assert.assertEquals("Product result page with iphone color varient " + colorVarientMidnightGreen
							+ " is Not opened", false, false);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void setZipCode(String zipCode) {

		try {
			productPageObjects.deliverTo().click();
			System.out.println("Sucessfully clicked on deliverTo Option on product page");

			System.out.println("Entering the zipcode");

			productPageObjects.inputFieldForAddingZipCode().click();

			productPageObjects.inputFieldForAddingZipCode().clear();

			productPageObjects.inputFieldForAddingZipCode().sendKeys(zipCode);

			System.out.println("After send the zip code as " + zipCode + " Click on Apply button");

			Actions actions = new Actions(driver);
			actions.moveToElement(productPageObjects.applyButtonOnLocationPopUp()).click().build().perform();
			System.out.println("After clicking on Apply button on location pop up");
			// productPageObjects.applyButtonOnLocationPopUp().click();

			try {
				if (productPageObjects.continueButtonOnLocationPopUp().isDisplayed()) {
					System.out.println("Continue button exists on location Pop UP");
					actions.moveToElement(productPageObjects.continueButtonOnLocationPopUp()).click().build().perform();
					System.out.println("After clicking on Continue button on location pop up");
					// productPageObjects.continueButtonOnLocationPopUp().click();
				}
			} catch (NoSuchElementException | StaleElementReferenceException ex) {
				System.out.println("Continue button is not present in locationPopup");
				ex.printStackTrace();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public int addProductToTheCart() {

		int QUANTITY_IN_THE_CART = 0;
		Actions action = new Actions(driver);
		JavascriptExecutor jse2 = (JavascriptExecutor)driver;
		try {

			try {
				if (productPageObjects.addToCartButtonOnProductPage().isDisplayed()) {
					System.out.println("Addtocart button on product page");
					action.moveToElement(productPageObjects.addToCartButtonOnProductPage()).click().build().perform();
					// productPageObjects.addToCartButtonOnProductPage().click();
					System.out.println("We have sucessfully clicked on addToCart Button on product page");
				}

				if (productPageObjects.noThanksButtonOnAdditionAddedProductPage().isDisplayed()) {
					System.out.println("No Thanks button on product pop up page");
					productPageObjects.noThanksButtonOnAdditionAddedProductPage().click();
					System.out.println("We have sucessfully clicked on No Thanks Button on product page");
				}

			} catch (NoSuchElementException | StaleElementReferenceException ex) {
				System.out.println("AddtoCart OR NoThanks button is not present in product page");
			}

			try {
				if (productPageObjects.cartButtonOnAdditionAddedProductPage().isDisplayed()) {
					System.out.println("cart button exists on product page Pop UP");
					// productPageObjects.cartButtonOnAdditionAddedProductPage().click();
					action.moveToElement(productPageObjects.cartButtonOnAdditionAddedProductPage()).click().build()
							.perform();
					System.out.println("We have sucessfully clicked on cart Button on product page");
				}
			} catch (NoSuchElementException ex) {
				System.out.println("Cart button is not present in locationPopup");
			}

			QUANTITY_IN_THE_CART = Integer.parseInt(productPageObjects.quantityCountInCart().getText());
			System.out.println("The quantity of product present in the cart is : " + QUANTITY_IN_THE_CART);
			Assert.assertEquals(QUANTITY_IN_THE_CART + " product is added in the cart", true, true);
			
			try {
				if (productPageObjects.proceedToCheckOutButtonOnProductPage().isDisplayed()) {
					System.out.println("proceed to checkout button exists on product page Pop UP");
					// productPageObjects.cartButtonOnAdditionAddedProductPage().click();
					action.moveToElement(productPageObjects.proceedToCheckOutButtonOnProductPage()).click().build()
					.perform();
					System.out.println("We have sucessfully clicked on proceed to checkout Button on product page");
				}
			} catch (NoSuchElementException ex) {
				System.out.println("procedd to checkout button is not present in locationPopup");
			}

		} catch (

		Exception e) {
			e.printStackTrace();
		}

		return QUANTITY_IN_THE_CART;
	}
}
