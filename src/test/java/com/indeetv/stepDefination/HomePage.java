package com.indeetv.stepDefination;

import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import org.apache.http.util.Asserts;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;

import com.indeetv.TestBase.TestBase;
import com.indeetv.pageObjects.HomePageObjects;

import junit.framework.Assert;

public class HomePage {

	HomePageObjects homePageObjects;
	private WebDriver driver;
	
	public HomePage() {
		
		this.driver = TestBase.driver;
		homePageObjects = new HomePageObjects(driver);
	}
	
	
	
	

	public void checkAmazonLoginPageDisplayed()
	{
		try
		{
			try
			{
				if(homePageObjects.signInHeadingInLoginPage().isDisplayed())
				{
					System.out.println("Login page is displayed");
					Assert.assertEquals("Login page is displyed sucessfully ", true, true);
				}
			}catch(NoSuchElementException | StaleElementReferenceException ex)
			{
				ex.printStackTrace();
			}
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	
	
	public void openAmazonHomePageScreen(String url) {

		try {
			
			System.out.println("Launch the URL..................................................");
			driver.get("https://amazon.com");
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			System.out.println("=====================================================================");
			System.out.println("sucessfully launched the URL");
			System.out.println("=====================================================================");
			
			if (homePageObjects.amazonHomePageLogo().isDisplayed()) {
				System.out.println("Amazon home page is displayed..");
				Assert.assertEquals("Amazon home page is displayed sucessfully", true, true);
			}
		} catch (Exception e) {
			System.out.println("Not able to open the amazon home page");
			Assert.assertEquals("Amazon home page is not displayed sucessfully", false, false);
		}
	}

	public void searchProductOnSearchBarAndClickOnSubmitButton(String productName) {
		try {
			System.out.println("The product to be searched on searchBar is : " + productName);

			homePageObjects.searchBar().click();

			homePageObjects.searchBar().clear();

			homePageObjects.searchBar().sendKeys(productName);

			System.out.println("Before clicking on search Button");
			// homePageObjects.searchBarSubmitButton().click();

			homePageObjects.cellPhonesAndAccessoriesOptionInDropDown().click();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void checksearchPageResultsAndCompareWithInputs(String colorVarientWhite, String colorVarientMidnightGreen) {

		String iphoneColorVarient = null;
		int counter = 0;
		try {
			if (homePageObjects.searchPageResults().size() > 0) {
				System.out.println("Search Page results is displayed sucessfully");

				for (int i = 0; i < homePageObjects.searchPageResults().size(); i++) {

					iphoneColorVarient = homePageObjects.searchPageResults().get(i).getText();
					System.out.println("iphone color varient is coming as : "+iphoneColorVarient);

					if (iphoneColorVarient.contains(colorVarientWhite)) {
						System.out.println(" Iphone color varient " + colorVarientWhite + " is available");
						Assert.assertEquals("Iphone color varient " + colorVarientWhite + " is available", true, true);
						counter++;
					} else if (iphoneColorVarient.contains(colorVarientMidnightGreen)) {
						System.out.println(" Iphone color varient " + colorVarientMidnightGreen + " is available");
						Assert.assertEquals(
								"Iphone color varient " + colorVarientMidnightGreen + " is available", true, true);
						counter++;
					} else {
						System.out.println("No color varient with " + colorVarientMidnightGreen + " and "
								+ colorVarientWhite + " is available");
						Assert.assertEquals("Both the color varient with " + colorVarientMidnightGreen + " and "
								+ colorVarientWhite + " is not available", false, false);
					}
				}

				if (counter == 2) {
					System.out.println("Both color varient with " + colorVarientMidnightGreen + " and "
							+ colorVarientWhite + " is available");
					Assert.assertEquals("Both the color varient with " + colorVarientMidnightGreen + " and "
							+ colorVarientWhite + " is available", true, true);
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
